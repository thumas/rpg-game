using Xunit;

namespace RPGGame
{
    public class UnitTest1
    {
        [Fact]
        public void Test_Hero_Level_At_Creation()
        {
            int actual = 1;
            Mage TestMage = new Mage("Tester");
            Assert.Equal(actual, TestMage.Level);

        }
        [Fact]
        public void TestHeroLevel_AtLevelUp_Level2()
        {
            int actual = 2;
            Mage TestMage = new Mage("Tester");
            TestMage.LevelUp();
            Assert.Equal(actual, TestMage.Level);

        }
        [Fact]
        public void TestMageAttributes_AtCreation()
        {
            int actualStrength = 1;
            int actualDexterity = 1;
            int actualIntelligence = 8;
            Mage TestHero = new Mage("Tester");
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }

        [Fact]
        public void TestHunterAttributes_AtCreation()
        {
            int actualStrength = 1;
            int actualDexterity = 7;
            int actualIntelligence = 1;
            Hunter TestHero = new Hunter("Tester");
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }
        [Fact]
        public void TestRogueAttributes_AtCreation()
        {
            int actualStrength = 2;
            int actualDexterity = 6;
            int actualIntelligence = 1;
            Rogue TestHero = new Rogue("Tester");
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }
        [Fact]
        public void TestWarriorAttributes_AtCreation()
        {
            int actualStrength = 5;
            int actualDexterity = 2;
            int actualIntelligence = 1;
            Warrior TestHero = new Warrior("Tester");
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }

        [Fact]
        public void TestMageAttributes_AtLevelUp_Level2()
        {
            int actualStrength = 1+1;
            int actualDexterity = 1+1;
            int actualIntelligence = 8+5;
            Mage TestHero = new Mage("Tester");
            TestHero.LevelUp();
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }
        [Fact]
        public void TestHunterAttributes_AtLevelUp_Level2()
        {
            int actualStrength = 1 + 1;
            int actualDexterity = 7 + 5;
            int actualIntelligence = 1 + 1;
            Hunter TestHero = new Hunter("Tester");
            TestHero.LevelUp();
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }
        [Fact]
        public void TestRogueAttributes_AtLevelUp_Level2()
        {
            int actualStrength = 2 + 1;
            int actualDexterity = 6 + 4;
            int actualIntelligence = 1 + 1;
            Rogue TestHero = new Rogue("Tester");
            TestHero.LevelUp();
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }
        [Fact]
        public void TestWarriorAttributes_AtLevelUp_Level2()
        {
            int actualStrength = 5 + 3;
            int actualDexterity = 2 + 2;
            int actualIntelligence = 1 + 1;
            Warrior TestHero = new Warrior("Tester");
            TestHero.LevelUp();
            Assert.True(TestHero.Stats.Strength == actualStrength && TestHero.Stats.Dexterity == actualDexterity && TestHero.Stats.Intelligence == actualIntelligence);

        }

        [Fact]
        public void TestEquipWeapon_ItemLevelTooHigh_InvalidWeaponLevel()
        {
            /// <summary>
            /// Tests the method that checks if the level is high enough. The Equip() itself didnt throw an exception since it invokes CheckReq()
            /// </summary>
            Warrior TestWarrior = new Warrior("Tester");
            Weapon TestSword = new Weapon() { ReqLevel = 2, WeaponType = WeaponType.Sword};
            Assert.Throws<ItemLevelNotMet>(() => TestWarrior.CheckReq(TestSword));
        }
        [Fact]
        public void TestEquipArmor_ItemLevelTooHigh_InvalidArmorLevel()
        {
            /// <summary>
            /// Tests the method that checks if the level is high enough. The Equip() itself didnt throw an exception since it invokes CheckReq()
            /// </summary>
            Warrior TestWarrior = new Warrior("Tester");
            Armor TestBody = new Armor() { ReqLevel = 2, ArmorType = ArmorType.Plate, Equipslot = EquipSlot.Head };
            Assert.Throws<ItemLevelNotMet>(() => TestWarrior.CheckReq(TestBody));
        }
        [Fact]
        public void TestEquipWeapon_WrongWeaponType_InvalidWeaponType()
        {
            /// <summary>
            /// Tests the method that checks if the type is correct. The Equip() itself didnt throw an exception since it invokes CheckProfic()
            /// </summary>
            Warrior TestWarrior = new Warrior("Tester");
            Weapon TestBow = new Weapon() { ReqLevel = 1, WeaponType = WeaponType.Bow};
            Assert.Throws<ItemTypeNotWearable>(() => TestWarrior.CheckProfic(TestBow));
        }
        [Fact]
        public void TestEquipArmor_WrongArmorType_InvalidArmorType()
        {
            /// <summary>
            /// Tests the method that checks if the type is correct. The Equip() itself didnt throw an exception since it invokes CheckProfic()
            /// </summary>
            Warrior TestWarrior = new Warrior("Tester");
            Armor TestHood = new Armor() { ReqLevel = 1, ArmorType = ArmorType.Cloth, Equipslot = EquipSlot.Head};
            Assert.Throws<ItemTypeNotWearable>(() => TestWarrior.CheckProfic(TestHood));
        }
        [Fact]
        public void TestEquipWeapon_ValidWeapon()
        {
            /// <summary>
            /// Since Equip() is void, I assert that the warriors equipped weapon is the same as the weapon created
            /// </summary>
            Warrior TestWarrior = new Warrior("Tester");
            Weapon TestSword = new Weapon() { ReqLevel = 1, WeaponType = WeaponType.Sword};
            TestWarrior.Equip(TestSword);
            Assert.True(TestWarrior.EquippedItems[EquipSlot.Weapon] == TestSword);
        }

        [Fact]
        public void TestEquipArmor_ValidArmor()
        {
            /// <summary>
            /// Since Equip() is void, I assert that the warriors equipped weapon is the same as the weapon created
            /// </summary>
            Warrior TestWarrior = new Warrior("Tester");
            Armor TestHelm = new Armor() { ReqLevel = 1, ArmorType = ArmorType.Plate, Equipslot = EquipSlot.Head};
            TestWarrior.Equip(TestHelm);
            Assert.True(TestWarrior.EquippedItems[EquipSlot.Head] == TestHelm);
        }

        [Fact]

        public void TestDPS_NoWeaponEquipped()
        {
            /// <summary>
            /// Tests DPS without any weapon equipped. Default damage without weapon is 1, times primary attribute
            /// </summary>
            double actualDPS = 1.0 *(1.0+ 5.0 / 100.0);
            Warrior TestWarrior = new Warrior("Tester");
            Assert.Equal(actualDPS, TestWarrior.CalculateDPS());
        }

        [Fact]

        public void TestDPS_AxeEquipped()
        {
            /// <summary>
            /// Tests DPS when an axe is equipped with damage 7 and atk speed 1.1. , damage calculated is (WeaponDMG*AtkSpped)(1+PrimaryAttribute/100)
            /// </summary>
            double actualDPS = (7 * 1.1 ) * (1.0 + 5.0 / 100.0);
            Warrior TestWarrior = new Warrior("Tester");
            Weapon TestAxe = new Weapon() { AttackSpeed = 1.1, DPS = 7, ReqLevel = 1 };
            TestWarrior.Equip(TestAxe);
            Assert.Equal(actualDPS, TestWarrior.CalculateDPS());
        }
        [Fact]

        public void TestDPS_AxeEquipped_ArmorEqupped()
        {
            /// <summary>
            /// Tests DPS when an axe is equipped with damage 7 and atk speed 1.1. And an armor piece which adds 1 to the primary attribute, damage calculated is (WeaponDMG*AtkSpped)(1+PrimaryAttribute/100)
            /// </summary>
            double actualDPS = (7 * 1.1) * (1.0 + (5.0+1.0) / 100.0);
            Warrior TestWarrior = new Warrior("Tester");
            Weapon TestAxe = new Weapon() { AttackSpeed = 1.1, DPS = 7, ReqLevel = 1 };
            Armor TestPlate = new Armor() { ArmorType = ArmorType.Plate, Equipslot = EquipSlot.Body, ReqLevel = 1, Intelligence = 0, Strength = 1, Dexterity = 0  };
            TestWarrior.Equip(TestAxe);
            TestWarrior.Equip(TestPlate);
            Assert.Equal(actualDPS, TestWarrior.CalculateDPS());
        }
    }
}