﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
    ///Enums that helps with game logic
{
    
        public enum EquipSlot
        {
            Head,
            Body,
            Legs,
            Weapon
        }
        public enum RPClass
        {
            Warrior,
            Rogue,
            Mage,
            Hunter
        }
        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
        public enum WeaponType
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }
}
