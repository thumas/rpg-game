﻿using System;

namespace RPGGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior Gamer = new Warrior("Thomas");

            Weapon TestAxe = new Weapon() { AttackSpeed = 1.1, DPS = 7, ReqLevel = 1 };
            Armor TestPlate = new Armor() { ArmorType = ArmorType.Plate, Name = "xd", Equipslot = EquipSlot.Body, ReqLevel = 1, Dexterity = 0, Intelligence = 0, Strength = 1 };
            Gamer.Equip(TestAxe);
            Gamer.Equip(TestPlate);
            Console.WriteLine(Gamer.CalculateDPS());
    }
    }
}
