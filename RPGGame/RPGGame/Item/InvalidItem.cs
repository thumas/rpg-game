﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public class ItemLevelNotMet:Exception
    {
        public ItemLevelNotMet(string message):base(message)
        {

        }
    }
    public class ItemTypeNotWearable:Exception
    {
        public ItemTypeNotWearable(string message):base(message)
        {

        }

    }
}
