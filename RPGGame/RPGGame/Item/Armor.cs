﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public class Armor : Item
    {
        public double Strength;
        public double Intelligence;
        public double Dexterity;
        public ArmorType ArmorType { get; set; }

        public Armor()
        {
        }

    }
}
