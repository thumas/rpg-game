﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public abstract class Item
        ///Abstract class, inherits Weapon or Item
    {
        public string Name { get; set; }
        public int ReqLevel { get; set; }
        public EquipSlot Equipslot { get; set; }

    }
}
