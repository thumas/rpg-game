﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public class Weapon:Item
    {
        public double AttackSpeed { get; set; }
        public double DPS{ get; set; }
        public WeaponType WeaponType { get; set; }

        public Weapon()
        {
            this.Equipslot = EquipSlot.Weapon;
            this.Name = "";
            this.ReqLevel = 0;
            this.AttackSpeed = 0;
            this.DPS = 0;
            this.WeaponType = WeaponType.Axe;
        }
    }
}
