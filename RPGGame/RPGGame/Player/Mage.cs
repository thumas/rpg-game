﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public class Mage : Character
    {
        public Mage(string Name) : base(Name)
        {
            this.Class = RPClass.Mage;
            this.Stats.Strength = 1;
            this.Stats.Dexterity = 1;
            this.Stats.Intelligence = 8;
            Console.WriteLine("Welcome, " + this.Name);
            Console.WriteLine("You are a "+this.Class+"! Str: " + this.Stats.Strength + " Dex: " + this.Stats.Dexterity + " Int: " + this.Stats.Intelligence);


        }

        public override void LevelUpPrimary()
        ///Overrides LevelUp in parent method and adds mage specific stats
        {
            this.Stats.Intelligence += 4;
            Console.WriteLine("Str: "+this.Stats.Strength+" Dex: "+this.Stats.Dexterity+" Int: "+this.Stats.Intelligence);
        }
        public override double GetPrimaryAttribute()
        ///Overrides in parent method to enable class-specific stats, in this case, Intelligence
        {
            return this.Stats.Intelligence;

        }

        public override bool CheckProfic(Item item)
        /// Checks if the item can be used based on class-specific proficencies 
        {
            if (item is Weapon)
                {
                    if (((Weapon)item).WeaponType == WeaponType.Staff || ((Weapon)item).WeaponType == WeaponType.Wand)
                    {
                        return true;
                    }
                    else
                    {
                        throw new ItemTypeNotWearable("Can't equip that kind of weapon");
                    }
                }
                if (item is Armor)
                {
                    if (((Armor)item).ArmorType == ArmorType.Cloth)
                    {
                        return true;
                    }
                    else
                    {
                        throw new ItemTypeNotWearable("Can't equip that kind of armor");
                    }
                }
                else
                    return false;


            }

        }



    }

