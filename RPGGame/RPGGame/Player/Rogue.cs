﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public class Rogue : Character
    {
        Dictionary<EquipSlot, Item> EquippedItems = new Dictionary<EquipSlot, Item>();
        public Rogue(string Name) : base(Name)
        {
            this.Class = RPClass.Rogue;
            this.Stats.Strength = 2;
            this.Stats.Dexterity = 6;
            this.Stats.Intelligence = 1;
            Console.WriteLine("Welcome, " + this.Name);
            Console.WriteLine("You are a "+this.Class+"! Str: " + this.Stats.Strength + " Dex: " + this.Stats.Dexterity + " Int: " + this.Stats.Intelligence);


        }

        public override void LevelUpPrimary()
        ///Overrides LevelUp in parent method and adds rogue specific stats
        {
            this.Stats.Dexterity += 3;
            Console.WriteLine("Str: " + this.Stats.Strength + " Dex: " + this.Stats.Dexterity + " Int: " + this.Stats.Intelligence);
        }
        public override double GetPrimaryAttribute()
        ///Overrides in parent method to enable class-specific stats, in this case, Dexterity
        {
            return this.Stats.Dexterity;
        }

        public override bool CheckProfic(Item item)
        /// Checks if the item can be used based on class-specific proficencies 
        {
            if (item is Weapon)
            {
                if (((Weapon)item).WeaponType == WeaponType.Dagger || ((Weapon)item).WeaponType == WeaponType.Sword)
                {
                    return true;
                }
                else
                {
                    throw new ItemTypeNotWearable("Can't equip that kind of weapon");
                }
            }
            if (item is Armor)
            {
                if (((Armor)item).ArmorType == ArmorType.Leather || ((Armor)item).ArmorType == ArmorType.Mail)
                {
                    return true;
                }
                else
                {
                    throw new ItemTypeNotWearable("Can't equip that kind of armor");
                }
            }
            else
                return false;


        }

    }


    }

