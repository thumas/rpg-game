﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
   
    public abstract class Character
    {
        public string Name;
        public int Level = 1;
        public Stats Stats { get; set; }

        public RPClass Class { get; set; }
        public object DPS { get; private set; }

        public Dictionary<EquipSlot, Item> EquippedItems = new Dictionary<EquipSlot, Item>();


        public Character (string Name)
        {
            this.Name = Name;
            this.Stats = new Stats ();

        }

        public void LevelUp()
            ///Adds one to each stat and calls the LevelUpPrimary for character-specific stats
        {
            this.Level += 1;
            Console.WriteLine("You leveled up to level " + this.Level + "!");
            this.Stats.Strength += 1;
            this.Stats.Dexterity += 1;
            this.Stats.Intelligence += 1;
            LevelUpPrimary();
        }
        public virtual void Equip(Item item)
            /// Attempts to Equip an item, checks the req level and proficencies. See invoked methods for more details
        {
            try
            {
                if (CheckReq(item) && CheckProfic(item))
                {

                    
                    if (item is Armor)
                    {
                        this.Stats.Strength += ((Armor)item).Strength;
                        this.Stats.Dexterity += ((Armor)item).Dexterity;
                        this.Stats.Intelligence += ((Armor)item).Intelligence;
                        try
                        {
                            this.Stats.Strength -= ((Armor)EquippedItems[item.Equipslot]).Strength;
                            this.Stats.Dexterity -= ((Armor)EquippedItems[item.Equipslot]).Dexterity;
                            this.Stats.Intelligence -= ((Armor)EquippedItems[item.Equipslot]).Intelligence;
                        }
                        catch { }
                       
                    }
                    EquippedItems[item.Equipslot] = item;
                    Console.WriteLine("You equipped " + item.Name);
                }
            }
            catch (ItemLevelNotMet ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ItemTypeNotWearable ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }
        
        public bool CheckReq(Item item)
            ///checks the items req level, returns true if the characters level is high enough
        {
            if (item.ReqLevel > this.Level)
            {
                throw new ItemLevelNotMet("This items required level is higher than your level");
                return false;
            }
            else
            {
                return true;
            }
        }
        public virtual bool CheckProfic(Item item)
            ///Virtual method that gets overloaded with class-specific proficencies
        {
            return false;
        }
        public virtual void LevelUpPrimary()
            ///virtual method that gets overloaded with class-specific stats
        {
        }
        public virtual double GetPrimaryAttribute()
            ///overloads and returns class primary attributes
        {
            return 0;
        }
        public double CalculateDPS()
            /// calculates the damage based on given rules
        {       
            try
            {
                double WepDMG = ((Weapon)EquippedItems[EquipSlot.Weapon]).DPS;
                double WepSpeed = ((Weapon)EquippedItems[EquipSlot.Weapon]).AttackSpeed;
                return (WepDMG*WepSpeed) * (1.0 + this.GetPrimaryAttribute() / 100.0);
            }catch
            {
                return 1 * (1 + this.GetPrimaryAttribute() / 100);
            }

        } 
        

    }
    
}


