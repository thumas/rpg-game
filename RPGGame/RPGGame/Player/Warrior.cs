﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame
{
    public class Warrior : Character
    {
        Dictionary<EquipSlot, Item> EquippedItems = new Dictionary<EquipSlot, Item>();
        public Warrior(string Name) : base(Name)
        {
            this.Class = RPClass.Warrior;
            this.Stats.Strength = 5;
            this.Stats.Dexterity = 2;
            this.Stats.Intelligence = 1;
            Console.WriteLine("Welcome, " + this.Name);
            Console.WriteLine("You are a " + this.Class + "! Str: " + this.Stats.Strength + " Dex: " + this.Stats.Dexterity + " Int: " + this.Stats.Intelligence);


        }

        public override void LevelUpPrimary()
            ///Overrides LevelUp in parent method and adds warrior specific stats
        {
            this.Stats.Strength += 2;
            this.Stats.Dexterity += 1;
            Console.WriteLine("Str: " + this.Stats.Strength + " Dex: " + this.Stats.Dexterity + " Int: " + this.Stats.Intelligence);
        }
        public override double GetPrimaryAttribute()
        ///Overrides in parent method to enable class-specific stats, in this case, Strength
        {
            return this.Stats.Strength;
        }
    

    public override bool CheckProfic(Item item)
            /// Checks if the item can be used based on class-specific proficencies 
    {
        if (item is Weapon)
        {
            if (((Weapon)item).WeaponType == WeaponType.Hammer || ((Weapon)item).WeaponType == WeaponType.Axe || ((Weapon)item).WeaponType == WeaponType.Sword)
            {
                return true;
            }
            else
            {
                throw new ItemTypeNotWearable("Can't equip that kind of weapon");
            }
        }
        if (item is Armor)
        {
            if (((Armor)item).ArmorType == ArmorType.Plate || ((Armor)item).ArmorType == ArmorType.Mail)
            {
                return true;
            }
            else
            {
                throw new ItemTypeNotWearable("Can't equip that kind of armor");
            }
        }
        else
            return false;


    } }

    }


    

